package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.demo.controller.DemoController;
import com.example.demo.model.BoardMerchant;
import com.example.demo.repo.demoRepo;
import com.example.demo.service.DemoService;

@ExtendWith(MockitoExtension.class)
class DemoApplicationTests {

	@InjectMocks
	DemoController demoController;

	@Mock
	demoRepo demoRepo;

	@Mock
	private DemoService demoService;
	
	private BoardMerchant boardMerchant1 = new BoardMerchant();
	
	private BoardMerchant boardMerchant2 = new BoardMerchant();
	
	private List<BoardMerchant> boardMerchantList = new ArrayList<>();
	
	@BeforeEach
	void setUp() throws Exception {
		boardMerchant1.setId(1L);
		boardMerchant1.setName("Andy");
		boardMerchant1.setLocation("Colorado Springs");
		boardMerchant1.setUuid("MFG1");
		boardMerchant2.setId(2L);
		boardMerchant2.setName("Billy");
		boardMerchant2.setLocation("Denver");
		boardMerchant2.setUuid("MFG2");
		boardMerchantList.add(boardMerchant1);
		boardMerchantList.add(boardMerchant2);
	}
	
	@Test
	void boardMerchantAllTest() {
		
		when(demoService.boardMerchantAll()).thenReturn(boardMerchantList);
		ResponseEntity<Object> response = demoController.boardMerchantAll();
		HttpStatus status = response.getStatusCode();
		ArrayList<BoardMerchant> responseArray = (ArrayList<BoardMerchant>) response.getBody();
		assertEquals(responseArray.get(0).getName(), "Andy");
		assertEquals(HttpStatus.OK, status);

	}

}
