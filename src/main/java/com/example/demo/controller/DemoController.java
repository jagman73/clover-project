package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.BoardMerchant;
import com.example.demo.model.UUIDResponse;
import com.example.demo.service.DemoService;

import io.swagger.annotations.ApiOperation;


@RestController


public class DemoController {


	@Autowired

	private DemoService demoService;


	/*
	 * @ApiOperation(value = "Get list of all Participants in the Database ",
	 * response = BirthDateMain.class, tags = "birthDateExtractAll")
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
	 * 
	 * @ApiResponse(code = 401, message = "Not authorized!"), @ApiResponse(code =
	 * 403, message = "Forbidden!!!"),
	 * 
	 * @ApiResponse(code = 404, message = "Not found!!!") })
	 */

	@ApiOperation(value = "Add Board Merchants to the Database ", response = BoardMerchant.class, tags = "boardMerchantAdd")
	@RequestMapping(method = RequestMethod.POST, path = "/api/v1/boardMerchantAdd")

	public ResponseEntity<Object> boardMerchantAdd(@RequestBody BoardMerchant boardMerchant) {

		UUIDResponse uuidResponse = demoService.boardMerchantAdd(boardMerchant);

		return new ResponseEntity<>(uuidResponse, HttpStatus.OK);

	}
	
	@ApiOperation(value = "Get list of all Board Merchants in the Database ", response = BoardMerchant.class, tags = "boardMerchantAll")
	@RequestMapping(method = RequestMethod.GET, path = "/api/v1/boardMerchantAll")
	
	public ResponseEntity<Object> boardMerchantAll() {


		Iterable<BoardMerchant> merchantsExtracted = demoService.boardMerchantAll();


		return new ResponseEntity<>(merchantsExtracted, HttpStatus.OK);

	}

	
}
