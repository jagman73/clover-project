package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter 
public class UUIDResponse {

	private String uuid;	
}