package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter 
@Setter 
public class BoardMerchant {

	@Id

	@Column(name="ID")

	private Long id;

	@Column(name="Name")

	private String name;

	@Column(name="Location")

	private String location;
	
	@Column(name="UUID")

	private String uuid;
	
}
