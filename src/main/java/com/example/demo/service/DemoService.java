package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.BoardMerchant;
import com.example.demo.model.UUIDResponse;
import com.example.demo.repo.demoRepo;

@Service
public class DemoService {

	@Autowired demoRepo demoRepo;
	
	public UUIDResponse boardMerchantAdd(BoardMerchant boardMerchant) {

		String addMessage = null;

		Long maxID = (long) demoRepo.findMaxID();
		 
		if (maxID == 0) {
		 
			maxID = 1L;
		 
		} else {
		
			maxID ++;  
		}
		
		boardMerchant.setId(maxID);
		String UUID = "MFG" + maxID;
		boardMerchant.setUuid(UUID);
		
		demoRepo.saveAndFlush(boardMerchant);
		
		addMessage = "Added id: " + boardMerchant.getId();

		UUIDResponse uuidResponse = new UUIDResponse();
		uuidResponse.setUuid(UUID);
		
		return uuidResponse;
			
	}	

	public Iterable<BoardMerchant> boardMerchantAll() {

		return demoRepo.findAll();

	}

	
}
