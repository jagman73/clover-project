package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.BoardMerchant;

@Repository

public interface demoRepo extends JpaRepository<BoardMerchant, Long>, demoRepoCustom {

}
