package com.example.demo.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

@Repository

public class demoRepoImpl implements demoRepoCustom{
	
	@PersistenceContext
	EntityManager entityManager;
		
	@Override
	public Long findMaxID() {
		long maxResult;
		
		TypedQuery<Long> query = entityManager.createQuery("SELECT MAX(id) FROM BoardMerchant", Long.class);
		
		try {
			maxResult = query.getSingleResult();
			
		} catch (NullPointerException e) { 
		
			maxResult = 0;
		}
		return maxResult;
		
	}
	
}

